module.exports = (app) ->
    # socket-io routes
    
    rooms = ["room1", "room2", "room3"]
    
    app.io.route "connection", (req) ->
        req.io.emit "hello", "Hello World!!!"
    
    app.io.route "ready", (req) ->
        req.session.name = req.data
        req.session.save ()->
            req.io.emit